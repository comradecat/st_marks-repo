﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ST_Marks.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ST_Marks.Controllers
{
    public class MarksController : Controller
    {
        private readonly AppDbContext _context;

        public MarksController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Marks
        public async Task<IActionResult> Index()
        {
            var Marks = await _context.Marks.Include(m => m.Student).Include(m => m.Course).ToListAsync();

            var uniqueStudents = Marks.Select(p => p.Student).Distinct();

            List<MarksViewModel> model = new List<MarksViewModel>();

            foreach (var student in uniqueStudents)
            {
                MarksViewModel mvm = new MarksViewModel
                {
                    StudentId = student.Id,
                    StudentName = $"{student.FirstName} {student.LastName}"
                };

                var relatedMarks = Marks.Where(m => m.StudentId == student.Id);
                List<CourseMarksViewModel> courseMarks = new List<CourseMarksViewModel>();
                foreach (var relm in relatedMarks)
                {
                    var cm = new CourseMarksViewModel
                    {
                        CourseId = relm.CourseId,
                        CourseName = relm.Course.Name,
                        MarkId = relm.Id,
                        Mark = relm.Grade
                    };

                    courseMarks.Add(cm);
                }

                mvm.CourseMarks = courseMarks;
                model.Add(mvm);
            }

            return View(model);
        }
        

        [HttpPost]
        public async Task<ActionResult> Change(int Id, int Value)
        {
            try
            {
                var Mark = await _context.Marks.Where(m => m.Id == Id).ToListAsync();
                Mark.First().Grade = Value;

                await _context.SaveChangesAsync();

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
    }
}