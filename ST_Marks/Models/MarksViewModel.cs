﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ST_Marks.Models
{
    public class MarksViewModel
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        public List<CourseMarksViewModel> CourseMarks { get; set; }
    }
}
