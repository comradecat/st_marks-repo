﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ST_Marks.Models
{
    public class CourseMarksViewModel
    {
        public int CourseId { get; set; }
        public int MarkId { get; set; }

        public string CourseName { get; set; }
        public int? Mark { get; set; }
    }
}
