﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ST_Marks.Models
{
    public class Course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; } //str ako je kombinacija slova i brojeva kao IT101

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        //fk
        public ICollection<Mark> Marks { get; set; }
    }
}
